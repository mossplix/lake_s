from plone.app.testing import PloneSandboxLayer
from plone.app.testing import applyProfile
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import IntegrationTesting
from plone.app.testing import FunctionalTesting

from zope.configuration import xmlconfig

class LakeTheme(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)
    
    def setUpZope(self, app, configurationContext):
        # Load ZCML
        import lake.theme
        xmlconfig.file('configure.zcml', lake.theme, context=configurationContext)
    
    def setUpPloneSite(self, portal):
        applyProfile(portal, 'lake.theme:default')

LAKE_THEME_FIXTURE = LakeTheme()
LAKE_THEME_INTEGRATION_TESTING = IntegrationTesting(bases=(LAKE_THEME_FIXTURE,), name="LakeTheme:Integration")
LAKE_THEME_FUNCTIONAL_TESTING = FunctionalTesting(bases=(LAKE_THEME_FIXTURE,), name="LakeTheme:Functional")
