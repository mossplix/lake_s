import unittest2 as unittest
from lake.policy.testing import LAKE_POLICY_INTEGRATION_TESTING

class TestSetup(unittest.TestCase):

    layer = LAKE_POLICY_INTEGRATION_TESTING

    def test_portal_title(self):
        portal = self.layer['portal']
        self.assertEqual("Lake Skader", portal.getProperty('title'))

    def test_portal_description(self):
        portal = self.layer['portal']
        self.assertEqual("Welcome to Lake Skader", portal.getProperty('description'))