from plone.app.testing import PloneSandboxLayer
from plone.app.testing import applyProfile
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import IntegrationTesting

from zope.configuration import xmlconfig

class LakePolicy(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load ZCML
        import lake.policy
        xmlconfig.file('configure.zcml', lake.policy, context=configurationContext)

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'lake.policy:default')

LAKE_POLICY_FIXTURE = LakePolicy()
LAKE_POLICY_INTEGRATION_TESTING = IntegrationTesting(bases=(LAKE_POLICY_FIXTURE,), name="Lake:Integration")