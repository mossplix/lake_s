from setuptools import setup, find_packages
import os

version = '1.0'

setup(name='lake.content',
      version=version,
      description="",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?:action=list_classifiers
      classifiers=[
        "Framework :: Plone",
        "Programming Language :: Python",
        ],
      keywords='',
      author='Mugisha Moses,Malthe Borch',
      author_email='mossplix@gmail.com,mborch@gmail.com',
      url='http://lake-s.info',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['lake'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          'Products.CMFPlone',
          'plone.app.dexterity [grok]',
          'plone.app.referenceablebehavior',
          'plone.app.relationfield',
          'plone.namedfile [blobs]',#blob support
          'archetypes.schemaextender',
          'plone.app.registry',
          

          # -*- Extra requirements: -*-
      ],
      extras_require={
      'test': ['plone.app.testing',]
      },

      entry_points="""
      # -*- Entry points: -*-

      [z3c.autoinclude.plugin]
      target = plone
      """,
      setup_requires=["PasteScript"],
      paster_plugins=["ZopeSkel"],
      )
